# OpenML dataset: insurance_dataset

https://www.openml.org/d/43157

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Dataset description**

Insurance is a network for evaluating car insurance risks.


**Format of the dataset**


The insurance data set contains the following 27 variables:

GoodStudent (good student): a two-level factor with levels False and True.

Age (age): a three-level factor with levels Adolescent, Adult and Senior.

SocioEcon (socio-economic status): a four-level factor with levels Prole, Middle, UpperMiddle and Wealthy.

RiskAversion (risk aversion): a four-level factor with levels Psychopath, Adventurous, Normal and Cautious.

VehicleYear (vehicle age): a two-level factor with levels Current and older.

ThisCarDam (damage to this car): a four-level factor with levels None, Mild, Moderate and Severe.

RuggedAuto (ruggedness of the car): a three-level factor with levels EggShell, Football and Tank.

Accident (severity of the accident): a four-level factor with levels None, Mild, Moderate and Severe.

MakeModel (car's model): a five-level factor with levels SportsCar, Economy, FamilySedan, Luxury and SuperLuxury.

DrivQuality (driving quality): a three-level factor with levels Poor, Normal and Excellent.

Mileage (mileage): a four-level factor with levels FiveThou, TwentyThou, FiftyThou and Domino.

Antilock (ABS): a two-level factor with levels False and True.

DrivingSkill (driving skill): a three-level factor with levels SubStandard, Normal and Expert.

SeniorTrain (senior training): a two-level factor with levels False and True.

ThisCarCost (costs for the insured car): a four-level factor with levels Thousand, TenThou, HundredThou and Million.

Theft (theft): a two-level factor with levels False and True.

CarValue (value of the car): a five-level factor with levels FiveThou, TenThou, TwentyThou, FiftyThou and Million.

HomeBase (neighbourhood type): a four-level factor with levels Secure, City, Suburb and Rural.

AntiTheft (anti-theft system): a two-level factor with levels False and True.

PropCost (ratio of the cost for the two cars): a four-level factor with levels Thousand, TenThou, HundredThou and Million.

OtherCarCost (costs for the other car): a four-level factor with levels Thousand, TenThou, HundredThou and Million.

OtherCar (other cars involved in the accident): a two-level factor with levels False and True.

MedCost (cost of the medical treatment): a four-level factor with levels Thousand, TenThou, HundredThou and Million.

Cushioning (cushioning): a four-level factor with levels Poor, Fair, Good and Excellent.

Airbag (airbag): a two-level factor with levels False and True.

ILiCost (inspection cost): a four-level factor with levels Thousand, TenThou, HundredThou and Million.

DrivHist (driving history): a three-level factor with levels Zero, One and Many.

**Source **

Binder J, Koller D, Russell S, Kanazawa K (1997). "Adaptive Probabilistic Networks with Hidden Variables". Machine Learning, 29(2-3):213-244.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43157) of an [OpenML dataset](https://www.openml.org/d/43157). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43157/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43157/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43157/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

